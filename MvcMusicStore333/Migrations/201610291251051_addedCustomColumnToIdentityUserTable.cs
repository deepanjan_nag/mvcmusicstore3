namespace MvcMusicStore333.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCustomColumnToIdentityUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "AddedColumn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "AddedColumn");
        }
    }
}
