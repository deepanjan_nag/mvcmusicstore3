﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMusicStore333.Models
{
    public class ShoppingCartViewModel
    {
        public List<Cart>   CartItems   { get; set; }
        public decimal      CartTotal       { get; set; }
    }
}