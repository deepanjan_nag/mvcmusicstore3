﻿using Microsoft.AspNet.Identity.EntityFramework;
using MvcMusicStore333.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcMusicStore333.Models
{
    public class Context : ApplicationDbContext   //Instead of DbContext
    {
        //public Context() : base("MvcMusicStore3") { }
        public DbSet<Album>         Albums          { get; set; }
        public DbSet<Genre>         Genres          { get; set; }
        public DbSet<Artist>        Artists         { get; set; }
        public DbSet<Cart>          Carts           { get; set; }
        public DbSet<Order>         Orders          { get; set; }
        public DbSet<OrderDetail>   OrderDetails    { get; set; }     
    }
}