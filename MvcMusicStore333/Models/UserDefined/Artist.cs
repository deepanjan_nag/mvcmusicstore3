﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcMusicStore333.Models
{
    public class Artist
    {
        public int      ArtistId    { get; set; }
        public string   Name        { get; set; }
    }
}