﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore333.Models
{
    [Bind(Exclude = "AlbumId")]
    public class Album
    {
        [ScaffoldColumn(false)]
        public          int     AlbumId     { get; set; }
        [Required(ErrorMessage = "An Album Title is required")]
        [StringLength(160)]
        public          string  Title       { get; set; }
        [Required(ErrorMessage = "Price is required")]
        [Range(0.01, 100.00, ErrorMessage = "Price must be between 0.01 and 100.00")]
        public          decimal Price       { get; set; }
        [DisplayName("Album Art URL")]
        [StringLength(1024)]
        public          string  AlbumArtUrl { get; set; }


        [DisplayName("Genre")]
        public virtual  int                 GenreId         { get; set; }
        public virtual  Genre               Genre           { get; set; }
        [DisplayName("Artist")]
        public virtual  int                 ArtistId        { get; set; }
        public virtual  Artist              Artist          { get; set; }
        public virtual List<OrderDetail>    OrderDetails    { get; set; }
    }
}