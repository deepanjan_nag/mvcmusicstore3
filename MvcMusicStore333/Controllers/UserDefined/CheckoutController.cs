﻿using MvcMusicStore333.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore333.Controllers.UserDefined
{
    [Authorize]
    public class CheckoutController : Controller
    {
        Context context = new Context();
        const string PromoCode = "FREE";
        [Authorize]
        public ActionResult AddressAndPayment()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddressAndPayment(FormCollection formValues)
        {
            var order = new Order();
            TryUpdateModel(order);

            try
            {
                if (string.Equals(formValues["PromoCode"], PromoCode, StringComparison.OrdinalIgnoreCase) == false)
                    return View(order);
                else
                {
                    order.Username = User.Identity.Name;
                    order.OrderDate = DateTime.Now;

                    //Save Order
                    context.Orders.Add(order);
                    context.SaveChanges();

                    //Process the order
                    var cart = ShoppingCart.GetCart(HttpContext);
                    cart.CreateOrder(order);

                    return RedirectToAction("Complete", new { id = order.OrderId });
                }
            }
            catch
            {
                //Invalid - redisplay with errors
                return View(order);
            }
        }
        public ActionResult Complete(int id)
        {
            // Validate customer owns this order
            bool isValid = context.Orders.Any(o => o.OrderId == id && o.Username == User.Identity.Name);
            if (isValid)
                return View(id);
            else
                return View("Error");
        }
    }
            
}