﻿using MvcMusicStore333.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcMusicStore3.Controllers
{
    public class StoreController : Controller
    {
        Context context = new Context();
        // GET: Store
        public ActionResult Index()
        {
            var genres = context.Genres.ToList();
            return View(genres);
        }
        public ActionResult Browse(int genreId)
        {
            var genre = context.Genres.Include("Albums").Single(g => g.GenreId == genreId);
            return View(genre);
        }
        public ActionResult Details(int id)
        {
            var album = context.Albums.Find(id);
            return View(album);
        }
        // GET: /Store/GenreMenu
        [ChildActionOnly]
        public ActionResult GenreMenu()
        {
            var genres = context.Genres.ToList();
            return PartialView(genres);
        }
    }
}